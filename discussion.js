/*
CRUD
	-create, read, update and delete

CREATE
	- allows us to create documents under a collection or create a collection if it does not exist yet

	//syntax
	//db.collections.insertOne()
		// collections - stands for collection name



*/


db.users.insertOne(
	{
		"firstName": "Tony",
		"lastName": "Stark",
		"username": "iAmIronMan",
		"email": "iloveyou3000@mail.com",
		"password": "starkIndustries",
		"isAdmin" : true

	}

	)

// db.collections.insertMany()
	// it allows us to insert or create two or more documents.

db.users.insertMany(
		[
		{
			"firstName": "Pepper",
			"lastName": "Potts",
			"username": "rescueArmore",
			"email": "pepper@mail.com",
			"password": "whereIsTonyAgain",
			"isAdmin" : false
		},
		{
			"firstName": "Steve",
			"lastName": "Rogers",
			"username": "theCaptain",
			"email": "captAmerica@mail.com",
			"password": "iCanLiftMjolnirToo",
			"isAdmin" : false
		},
		{
			"firstName": "Thor",
			"lastName": "Odinson",
			"username": "mightyThor",
			"email": "ThorNotLoki@mail.com",
			"password": "iAmWorthyToo",
			"isAdmin" : false
		},
		{
			"firstName": "Loki",
			"lastName": "Odinson",
			"username": "godOfMischief",
			"email": "loki@mail.com",
			"password": "iAmReallyLoki",
			"isAdmin" : false
		},

	])


db.courses.insertMany(
		[
		{
			"name": "Javascript",
			"price": 3500,
			"description": "Learn Javascript in a week!",
			"isActive" : true
		},
		{
			"name": "HTML",
			"price": 1000,
			"description": "Learn Basic HTML in 3 days!",
			"isActive" : true
		},
		{
			"name": "CSS",
			"price": 2000,
			"description": "Make your website fancy, learn CSS now!",
			"isActive" : true
		},
	
	])

/* READ
	- allows us to retrieve data
	- it needs a query or filters to specify the document we are retrieving.

		//syntax
			- db.collections.find()
				- allows us to retrieve ALL documents in the collection

*/


db.users.find( )


/*syntax

		db.collections.findOne({})
			-allows us to find the document that matches our criteria
*/


db.users.findOne({"isAdmin": false})
		


db.users.find({"lastName" : "Odinson", "firstName" :"Loki"})
	//it allows us to find the document that satisfies all criterias


/*UPDATE
	- allows us to update documents
	- also uses criteria or filter
	- $set operator
	REMINDER : UPDATES ARE PERMANENT AND CANT BE ROLLED BACK

		syntax:
			db.collections.updateOne({criteria: value}, {$set: {}})
				- allows us to update one document that satisfies the criteria

*/

db.users.updateOne({"lastName" : "Potts"}, {$set: {"lastName": "Stark"}})



/*
	syntax:
		// db.collections.updateMany({criteria: value}, {$set: {fieldToBeUpdated: updatedValue}})
			- allows us to update ALL documents that satisfy the criteria

*/

db.users.updateMany({"lastName": "Odinson"}, {$set: {"isAdmin": true}})




/*syntax:
	// db.collection.update({}, {$set: {fieldToBeUpdated: updated}})
		- allows us to update the first item in the collection

*/


db.users.updateOne({}, {$set: {"email": "starkindustries@mail.com"}})


db.courses.updateOne({"name": "Javascript"}, {$set: {"isActive": false}})



/*new field on courses*/


db.courses.updateMany({}, {$set : {"enrollees" : 10}})



/*DELETE
	// allows us to delete documents
	// provide criteria or filters to specify which document to delete from the collection.

	REMINDER: be careful when deleting documents because it is complicated to retrieve them back again

	syntax:
		db.collections.deleteOne({criteria: value})
			-allows us to delete the first item that matches our criteria

*/


db.users.deleteOne({"isAdmin": false}) //deleted potts because first item with false




db.users.deleteMany({"lastName" : "Odinson"}) // deletes all with odinson

db.users.deleteOne({}) // deletes the fiorst document



db.users.deleteMany({})